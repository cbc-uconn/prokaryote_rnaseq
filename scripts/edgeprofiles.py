from Bio import SeqIO
import argparse
parser = argparse.ArgumentParser()

parser.add_argument("-i", "--inputanno", help="Input annotation file in genbank format")
parser.add_argument("-f", "--fastaout", help="Output fasta file")
parser.add_argument("-p", "--pttout", help="Output ptt File name")
parser.add_argument("-r", "--rntout", help="Output rnt File name")
args = parser.parse_args()

print( "inputanno {} fastaout {} pttout {} rntout {} ".format(
        args.inputanno,
        args.fastaout,
        args.pttout,
        args.rntout
        ))

annotation_file = args.inputanno
rnt_file = args.rntout
ptt_file = args.pttout
fasta_file = args.fastaout
r = SeqIO.parse(annotation_file, "gb")
for record in r:
   fasta_file = open(fasta_file, "a")
   SeqIO.write(record, fasta_file, "fasta")
   fasta_file.close()
   record.features = [f for f in record.features if f.type == "rRNA" or f.type == "tRNA"]
   fout = open(rnt_file, "a")
   fout.write("{0} - 0..{1}\n".format(record.description, len(record)))
   fout.write("{0} RNAs\n".format(len(record.features)))
   fout.write("Location\tStrand\tLength\tPID\tGene\tSynonym Code\tCOG\tProduct\n")
   strand = {1:'+', -1:'-'}
   for f in record.features:
       fout.write("{0}\n".format("\t".join([str(f.location.start)+".."+str(f.location.end),strand[f.strand],str(abs(f.location.start-f.location.end)),'-',f.qualifiers["locus_tag"][0],f.qualifiers["locus_tag"][0],"-",f.qualifiers["product"][0]])))
   fout.close()

r = SeqIO.parse(annotation_file, "gb")
for record in r:
   record.features = [f for f in record.features if f.type == "CDS"]
   fout = open(ptt_file, "a")
   fout.write("{0} - 0..{1}\n".format(record.description, len(record)))
   fout.write("{0} proteins\n".format(len(record.features)))
   fout.write("Location\tStrand\tLength\tPID\tGene\tSynonym Code\tCOG\tProduct\n")
   for f in record.features:
       fout.write("{0}\n".format("\t".join([str(f.location.start)+".."+str(f.location.end),strand[f.strand],str(abs(f.location.start-f.location.end)),'-',f.qualifiers["locus_tag"][0],f.qualifiers["locus_tag"][0],"-",f.qualifiers["product"][0]])))
   fout.close()
   
   ## Code Source : https://www.biostars.org/p/116540/
   